const mysql = require('mysql')

const ConnectDB = (callback) => {
  var connection = mysql.createConnection({
    host: 'database-1.cgoidfibb19q.us-east-2.rds.amazonaws.com',
    user: 'admin',
    password: 'password',
    database: 'smartapp'
  })
  connection.connect((error) => {
    if (error) {
      console.error('Error in connecting with DB: ' + error.message)
      // callback(isSuccess = false, errorObject)
      return callback(false, error)
    } else {
      console.log('Connected with ID: ' + connection.threadId)
      // callback(isSuccess = true, connectionObject)
      return callback(true, connection)
    }
  })

}

module.exports = ConnectDB